'use strict';
$(document).ready(function() {

  var btn = $('#slide-down-btn');
  var container = $('.slide-wrapper');

  init(btn,container);

});

function init(btn, container) {
  btn.on('click', function() {
    $(btn).toggleClass('clicked');
    $(container).toggle('slide');
  });
}
